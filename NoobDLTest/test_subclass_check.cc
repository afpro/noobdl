//
// by afpro.
//

#include <gtest/gtest.h>
#include <NoobDL/Utils/SubclassCheck.h>

namespace {
  class A { };

  class B : public A { };

  class C : public B { };

  class D { };
}

TEST(TestSubclassCheck, SubClassCheck) {
  static_assert(NoobDL::Utils::SubclassCheck<A, A>::Success(), "static assert failed");

  ASSERT_TRUE((NoobDL::Utils::SubclassCheck<A, A>::Success()));
  ASSERT_TRUE((NoobDL::Utils::SubclassCheck<A, B>::Success()));
  ASSERT_TRUE((NoobDL::Utils::SubclassCheck<A, C>::Success()));
  ASSERT_TRUE((NoobDL::Utils::SubclassCheck<B, C>::Success()));

  ASSERT_FALSE((NoobDL::Utils::SubclassCheck<A, D>::Success()));
  ASSERT_FALSE((NoobDL::Utils::SubclassCheck<B, D>::Success()));
  ASSERT_FALSE((NoobDL::Utils::SubclassCheck<C, D>::Success()));

  ASSERT_FALSE((NoobDL::Utils::SubclassCheck<B, A>::Success()));
  ASSERT_FALSE((NoobDL::Utils::SubclassCheck<C, A>::Success()));
  ASSERT_FALSE((NoobDL::Utils::SubclassCheck<C, B>::Success()));
}
