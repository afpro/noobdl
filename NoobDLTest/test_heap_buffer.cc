//
// by afpro.
//

#include <gtest/gtest.h>
#include <NoobDL/Buffer.h>

TEST(TestHeapBuffer, TestCreateAndDecRef) {
  NoobDL::RefPtr<NoobDL::Buffer> buf = new NoobDL::HeapBuffer(4);
  ASSERT_TRUE(buf->localAccessible());
  *(int *)buf->ptr = 1;
}
