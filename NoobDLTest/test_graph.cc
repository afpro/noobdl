//
// by afpro.
//

#include <gtest/gtest.h>
#include <NoobDL/Graph.h>
#include <NoobDL/Symbol.h>
#include <NoobDL/Syms/Placeholder.h>
#include <NoobDL/Syms/Add.h>
#include <NoobDL/Syms/Mul.h>
#include <NoobDL/Syms/Fill.h>

using namespace NoobDL;
using namespace Syms;
using namespace Utils;
