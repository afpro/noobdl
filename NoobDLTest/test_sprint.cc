//
// by afpro.
//

#include <gtest/gtest.h>
#include <NoobDL/Utils/Sprint.h>

TEST(TestSprint, CheckCorrect) {
  auto s = NoobDL::Utils::sprint("%d", 100);
  ASSERT_EQ(s, "100");
}
