//
// by afpro.
//

#include <NoobDL/Device.h>
#include <NoobDL/Buffer.h>
#include "Cpu.h"

using namespace NoobDL;

namespace {
  struct CpuDevice : Device {
    ~CpuDevice() override = default;

    RefPtr<Buffer> alloc(size_t size) override {
      return new HeapBuffer(size);
    }

    void copy(DeviceMemoryCopyType copyType, const RefPtr<Buffer>& to, size_t toOffset, const RefPtr<Buffer> from, size_t fromOffset, size_t size) override {
      memcpy(toOffset + reinterpret_cast<char*>(to.ptr()),
             fromOffset + reinterpret_cast<char*>(from.ptr()),
             size);
    }

    int getRecommendStreamCount() override {
#ifdef __ANDROID__
      return 2;
#else
      return 8;
#endif
    }

    RefPtr<StreamExecutor> createExecutor() override {
      // TODO: implement this
      return nullptr;
    }
  };
}

RefPtr<Device> NoobDL::createCpuDevice() {
  return new CpuDevice();
}
