#pragma once

#include <NoobDL/Device.h>

#if defined _WIN32 || defined __CYGWIN__
# ifdef NOOB_DL_CPU_BUILDING
#   ifdef __GNUC__
#     define NOOB_DL_CPU_API __attribute__ ((dllexport))
#   else
#     define NOOB_DL_CPU_API __declspec(dllexport)
#   endif
# else
#   ifdef __GNUC__
#     define NOOB_DL_CPU_API __attribute__ ((dllimport))
#   else
#     define NOOB_DL_CPU_API __declspec(dllimport)
#   endif
# endif
#else
# if NOOB_DL_CPU_BUILDING && __GNUC__ >= 4
#   define NOOB_DL_CPU_API __attribute__ ((visibility ("default")))
# else
#   define NOOB_DL_CPU_API
# endif
#endif

namespace NoobDL {
  NOOB_DL_CPU_API RefPtr<Device> createCpuDevice();
}
