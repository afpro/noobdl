//
// by afpro.
//

#pragma once

#include <atomic>
#include "Utils/DLL.h"
#include "Utils/SubclassCheck.h"

namespace NoobDL {
  /**
   * object managed by ref count, after new, ref=0,
   * when decRef(), if ref=0, delete self
   */
  class RefCountedObj {
  protected:
    NOOB_DL_API RefCountedObj();
    NOOB_DL_API virtual ~RefCountedObj();

  public:
    NOOB_DL_API void incRef();
    NOOB_DL_API void decRef();

    // delete this object, ignore ref count
    NOOB_DL_API void deleteIt();

  private:
    std::atomic_int mRef;
  };

  template <class T>
  class RefPtr {
    static_assert(Utils::SubclassCheck<RefCountedObj, T>::Success(),
      "RefPtr only hold RefCountedObj and it's sub class");
  public:
    using Self = RefPtr<T>;

    RefPtr(T* ptr = nullptr) : mPtr(ptr) { if (mPtr) mPtr->incRef(); }

    // copy constructor
    RefPtr(const Self& other) : mPtr(other.mPtr) { if (mPtr) mPtr->incRef(); }

    // move constructor
    RefPtr(Self&& other) : mPtr(other.mPtr) { other.mPtr = nullptr; }

    // copy
    Self& operator=(const Self& other) {
      if (mPtr) mPtr->decRef();
      mPtr = other.mPtr;
      if (mPtr) mPtr->incRef();
      return *this;
    }

    // move
    Self& operator=(Self&& other) {
      if (mPtr) mPtr->decRef();
      mPtr = other.mPtr;
      other.mPtr = nullptr;
      return *this;
    }

    // set ptr
    Self& operator=(T* ptr) {
      if (mPtr) mPtr->decRef();
      mPtr = other.mPtr;
      if (mPtr) mPtr->incRef();
      return *this;
    }

    // check
    operator bool() const {
      return mPtr != nullptr;
    }

    // use
    T* operator->() const {
      return mPtr;
    }

    T* ptr() const {
      return mPtr;
    }

    operator T*() const {
      return mPtr;
    }

  private:
    T* mPtr;
  };
}
