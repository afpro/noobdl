//
// by afpro.
//

#pragma once

#include <string>
#include <vector>
#include "RefCountedObj.h"
#include "DType.h"

namespace NoobDL {
  class Graph;

  class Symbol : public RefCountedObj {
  public:
    NOOB_DL_API static std::string autoName();

    Symbol(const std::string& op,
           const DType dtype,
           const std::vector<int>& shape,
           const std::vector<std::string> inputs,
           const std::string& name = "")
      : name(name.empty() ? autoName() : name), op(op), dtype(dtype), shape(shape), inputs(inputs) { }

  protected:
    virtual ~Symbol() = default;

  public:
    const std::string name;
    const std::string op;
    const DType dtype;
    const std::vector<int> shape;
    const std::vector<std::string> inputs;
  };
}
