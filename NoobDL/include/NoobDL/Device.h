//
// by afpro.
//

#pragma once

#include <functional>
#include <map>
#include <vector>
#include "RefCountedObj.h"
#include "DType.h"

namespace NoobDL {
  class Buffer;
  class Symbol;

  enum DeviceMemoryCopyType {
    LOCAL_TO_HOST,
    HOST_TO_LOCAL,
  };

  class StreamExecutor : public RefCountedObj {
  public:
    StreamExecutor() = default;
    virtual ~StreamExecutor() = default;

    virtual void scheduleRun(const RefPtr<Symbol>& symbol,
                             const std::map<std::string, RefPtr<Buffer>>& inputs,
                             const RefPtr<Buffer>& value) = 0;

    /**
     * @param currentDerivative may be empty for first step
     */
    virtual void scheduleDerivative(const RefPtr<Symbol>& symbol,
                                    const RefPtr<Buffer> &currentDerivative,
                                    const std::map<std::string, RefPtr<Buffer>>& inputs,
                                    const std::string &input,
                                    const RefPtr<Buffer>& value) = 0;

    virtual void scheduleCallback(const std::function<void()>& func) = 0;

    /**
     * throw exception during run or derivative
     */
    virtual void waitAllComplete() = 0;
  };

  class Device : public RefCountedObj {
  protected:
    Device() = default;
    virtual ~Device() = default;

  public:
    template <typename T>
    inline RefPtr<Buffer> alloc(size_t count, T fill) {
      return alloc(count * sizeof(fill), &fill, sizeof(fill));
    }

    // memory allocation
    virtual RefPtr<Buffer> alloc(size_t size, void *fill = nullptr, size_t fillSize = 0) = 0;

    // copy
    virtual void copy(DeviceMemoryCopyType copyType,
                      const RefPtr<Buffer>& to, size_t toOffset,
                      const RefPtr<Buffer> from, size_t fromOffset,
                      size_t size) = 0;

    // executor
    virtual int getRecommendStreamCount() = 0;
    virtual RefPtr<StreamExecutor> createExecutor() = 0;
  };
}
