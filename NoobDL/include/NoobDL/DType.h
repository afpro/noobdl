//
// by afpro.
//

#pragma once

#include <stdlib.h>

namespace NoobDL {
  enum DType {
    i32 = 1,
    f32,
  };

  template <DType dtype>
  struct DTypeInfo { };

  template <>
  struct DTypeInfo<i32> {
    using Type = int32_t;
    static constexpr size_t size() { return 4; }
  };

  template <>
  struct DTypeInfo<f32> {
    using Type = float;
    static constexpr size_t size() { return 4; }
  };

  union DTypeUnion {
    DTypeInfo<i32>::Type i32;
    DTypeInfo<f32>::Type f32;
  };

#define NOOB_DL_DTYPE_UNION_CTOR(DTYPE) \
  DTypeUnion dtypeUnion_##DTYPE(DTypeInfo<DTYPE>::Type DTYPE) {\
    DTypeUnion u;\
    u.DTYPE = DTYPE;\
    return u;\
  }
  NOOB_DL_DTYPE_UNION_CTOR(i32)
  NOOB_DL_DTYPE_UNION_CTOR(f32)
#undef NOOB_DL_DTYPE_UNION_CTOR

  inline size_t dtypeSize(DType dtype) {
    switch (dtype) {
    case i32: return DTypeInfo<i32>::size();
    case f32: return DTypeInfo<f32>::size();
    }
    return 0;
  }
}
