//
// by afpro.
//

#pragma once

#include <map>
#include <vector>
#include <mutex>
#include <functional>
#include "Utils/DLL.h"
#include "RefCountedObj.h"

namespace NoobDL {
  class Symbol;

  class Graph : public RefCountedObj {
  public:
    Graph() = default;
    Graph(Graph&& graph) = default;
    Graph(const Graph& graph) = default;

  protected:
    ~Graph() override = default;

  public:
    /**
     * synchronized on this graph, use it on concurrent environment
     */
    NOOB_DL_API void syncRun(const std::function<void(Graph&)>& fun);

    NOOB_DL_API void addSymbol(RefPtr<Symbol> symbol);
    NOOB_DL_API size_t symbolCount() const;
    NOOB_DL_API RefPtr<Symbol> getSymbol(int index) const;
    NOOB_DL_API RefPtr<Symbol> getSymbol(const std::string& name) const;
    NOOB_DL_API std::vector<std::string> inputOf(const std::string& name) const;
    NOOB_DL_API std::vector<std::string> outputOf(const std::string& name) const;

  private:
    std::recursive_mutex mMut;
    std::vector<RefPtr<Symbol>> mSymbols;
    std::map<std::string, Symbol*> mSymbolMap;
    std::map<std::string, std::vector<std::string>> mOutputMap;
  };
}
