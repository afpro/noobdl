//
// by afpro.
//

#pragma once

#include "../Symbol.h"
#include "../Buffer.h"

namespace NoobDL { namespace Syms {
  class Variable : public Symbol {
  public:
    static constexpr const char* opName() { return "variable"; }

    Variable(bool trainable, DType dtype, const std::vector<int>& shape, const std::string& name = "")
      : Symbol(opName(), dtype, shape, inputs, name), trainable(trainable) { }

  protected:
    ~Variable() override = default;

  public:
    const bool trainable;
    const RefPtr<HeapBuffer> data;
  };
}}
