//
// by afpro.
//

#pragma once

#include "../Utils/DLL.h"
#include "../Symbol.h"

namespace NoobDL { namespace Syms {
  class Placeholder : public Symbol {
  public:
    static constexpr const char* opName() { return "Placeholder"; }

    Placeholder(DType dtype, const std::vector<int>& shape, const std::string& name = "")
      : Symbol(opName(), dtype, shape, {}, name) { }

  protected:
    ~Placeholder() override = default;
  };
}}
