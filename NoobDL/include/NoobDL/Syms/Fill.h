//
// by afpro.
//

#pragma once

#include "../Symbol.h"

namespace NoobDL { namespace Syms {
  class Fill : public Symbol {
  public:
    static constexpr const char* opName() { return "Fill"; }

    Fill(DType dtype, DTypeUnion value, const std::vector<int>& shape, const std::string& name = "")
      : Symbol(opName(), dtype, shape, {}, name), value(value) { }

  protected:
    ~Fill() override = default;

  public:
    const DTypeUnion value;
  };
}}
