//
// by afpro.
//

#pragma once

#include "../Symbol.h"

namespace NoobDL { namespace Syms {
  /**
   * mul each element
   * may be used as Derivative tmp operation, scheduler should *NOT* depends on symbol inputs names
   */
  class Mul : public Symbol {
  public:
    static constexpr const char* opName() { return "Mul"; }

    Mul(DType dtype, const std::vector<int>& shape, const std::vector<std::string>& inputs,
        const std::string& name = "")
      : Symbol(opName(), dtype, shape, inputs, name) { }

  protected:
    ~Mul() override = default;
  };
}}
