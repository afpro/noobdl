//
// by afpro.
//

#pragma once

#include "../Symbol.h"

namespace NoobDL { namespace Syms {
  /**
   * add each element
   * may be used as Derivative tmp operation, scheduler should *NOT* depends on symbol inputs names
   */
  class Add : public Symbol {
  public:
    static constexpr const char* opName() { return "Add"; }

    Add(DType dtype, const std::vector<int>& shape, const std::vector<std::string>& inputs,
        const std::string& name = "")
      : Symbol(opName(), dtype, shape, inputs, name) { }

  protected:
    ~Add() override = default;
  };
}}
