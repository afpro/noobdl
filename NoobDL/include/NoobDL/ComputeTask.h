//
// by afpro.
//

#pragma once

#include <functional>
#include <list>
#include <map>
#include <thread>
#include "RefCountedObj.h"

namespace NoobDL {
  class Buffer;
  class Device;
  class Graph;

  /**
   * *NOT* thread safe, change graph variable during compute cause undefined behaviour.
   */
  class ComputeTask {
    struct Task;
  public:
    using FetchCallback = std::function<void(const std::string& name, const RefPtr<Buffer>& data)>;
    using DerivativeCallback = std::function<void(const std::string& name, const std::string& target, const RefPtr<Buffer>& data)>;

    NOOB_DL_API ComputeTask(const RefPtr<Device>& device, const RefPtr<Graph>& graph);
    NOOB_DL_API ~ComputeTask();

    NOOB_DL_API ComputeTask& feed(const std::string& name, const RefPtr<Buffer>& data);
    NOOB_DL_API ComputeTask& fetch(const std::string& name, const FetchCallback& callback);
    NOOB_DL_API ComputeTask& derivative(const std::string& name, const std::string& target, const DerivativeCallback& callback);
    NOOB_DL_API void run() const;

  private:
    NOOB_DL_API void checkThreadAndExecuteStatus(const char* message, bool desireExecuted = false) const;
    NOOB_DL_API Task* expandCompute(const std::string &name);
    NOOB_DL_API Task* expandDerivative(const std::string &name, const std::string &target, const std::string &current);

  public:
    const RefPtr<Device> device;
    const RefPtr<Graph> graph;

  private:
    std::thread::id mOriginThread;
    bool mExecuted;
    std::map<std::string, RefPtr<Buffer>> mFeeds;
    std::vector<Task*> mTasks;
  };
}
