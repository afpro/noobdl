//
// by afpro.
//

#pragma once

#include <stdio.h>
#include <string>
#include <stdexcept>

namespace NoobDL { namespace Utils {
  template <typename...Args>
  std::string sprint(const Args&...args) {
    // get length
    int len = snprintf(nullptr, 0, args...);
    if (len <= 0)
      return "";

    // prepare memory
    std::string s;
    s.resize(len + 1);

    // print
    snprintf((char *)s.data(), s.size(), args...);

    // remove terminate '\0'
    s.resize(len);

    // return
    return s;
  }

  constexpr const char* boolStr(bool value) {
    return value ? "true" : "false";
  }

  template <typename...Args>
  void runtimeError(const Args&...args) {
    throw std::runtime_error(sprint(args...));
  }
}}
