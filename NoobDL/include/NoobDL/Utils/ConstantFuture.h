//
// by afpro.
//

#pragma once

#include <future>

namespace NoobDL { namespace Utils {
  template <typename T>
  std::future<T> constantFuture(const T& t) {
    std::promise<T> promise;
    promise.set_value(t);
    return promise.get_future();
  }

  template <typename T>
  std::future<T> constantFuture(T&& t) {
    std::promise<T> promise;
    promise.set_value(std::move(t));
    return promise.get_future();
  }

  template <typename T>
  std::future<T> constantFuture(T& t) {
    std::promise<T> promise;
    promise.set_value(t);
    return promise.get_future();
  }
}}
