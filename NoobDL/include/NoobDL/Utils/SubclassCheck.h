//
// by afpro.
//

#pragma once

namespace NoobDL { namespace Utils {
  /**
   * a type check trick(use method overload), at http://stackoverflow.com/a/7020364
   */
  template <class Base, class Derived>
  struct SubclassCheck {
    static constexpr bool Test(Base*) { return true; }
    static constexpr bool Test(...) { return false; }
    static constexpr bool Success() { return Test((Derived *)nullptr); }
  };
}}
