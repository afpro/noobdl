//
// by afpro.
//

#pragma once

#if defined _WIN32 || defined __CYGWIN__
# ifdef NOOB_DL_BUILDING
#   ifdef __GNUC__
#     define NOOB_DL_API __attribute__ ((dllexport))
#   else
#     define NOOB_DL_API __declspec(dllexport)
#   endif
# else
#   ifdef __GNUC__
#     define NOOB_DL_API __attribute__ ((dllimport))
#   else
#     define NOOB_DL_API __declspec(dllimport)
#   endif
# endif
#else
# if NOOB_DL_BUILDING && __GNUC__ >= 4
#   define NOOB_DL_API __attribute__ ((visibility ("default")))
# else
#   define NOOB_DL_API
# endif
#endif
