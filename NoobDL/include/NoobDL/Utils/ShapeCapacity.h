//
// by afpro.
//

#pragma once

#include <vector>

namespace NoobDL { namespace Utils {
  inline int shapeCapacity(const std::vector<int>& shape) {
    int c = 1;
    for (int dim : shape)
      c *= dim;
    return c;
  }
}}
