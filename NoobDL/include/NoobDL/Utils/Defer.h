//
// by afpro.
//

#pragma once

#include <functional>

#define NOOB_DL_DEFER(FUNC) NOOB_DL_DEFER_HELPER1(__LINE__, FUNC)
#define NOOB_DL_DEFER_HELPER1(LINE, FUNC) NOOB_DL_DEFER_HELPER2(LINE, FUNC)
#define NOOB_DL_DEFER_HELPER2(LINE, FUNC) ::NoobDL::Utils::DeferHelper __defer__##LINE( FUNC );
#define NOOB_DL_DEFER_BLOCK(BLOCK) NOOB_DL_DEFER([&]() { BLOCK; })

namespace NoobDL { namespace Utils {
  class DeferHelper {
  public:
    DeferHelper(const std::function<void()>& func) : mFunc(func) {}
    ~DeferHelper() { if (mFunc) mFunc(); }

  private:
    std::function<void()> mFunc;
  };
}}
