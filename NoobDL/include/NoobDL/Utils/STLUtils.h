//
// by afpro.
//

#pragma once

#include <list>
#include <map>
#include <functional>

namespace NoobDL { namespace Utils {
  template <typename Container, typename T>
  bool contains(const Container& container, const T& t) {
    return container.find(t) != container.end();
  }

  template <typename K, typename V>
  bool mapContains(const std::map<K, V>& map, const K& k) {
    return map.find(k) != map.end();
  }

  template <typename Ret, typename K, typename V>
  Ret findMap(const std::map<K, V>& map, const K& k, const std::function<Ret(const V&)>& success, const std::function<Ret()>& fail) {
    auto iter = map.find(k);
    if (iter == map.end())
      return fail();
    return success(iter->second);
  }
}}
