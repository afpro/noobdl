//
// by afpro.
//

#pragma once

#include "Utils/DLL.h"
#include "RefCountedObj.h"

namespace NoobDL {
  class Buffer : public RefCountedObj {
  public:
    /**
     * @return if ptr can't be used directly (eg. for heap, true; for cuda, false)
     */
    virtual bool localAccessible() = 0;

  protected:
    Buffer(void* ptr, size_t size) : ptr(ptr), size(size) { }
    virtual ~Buffer() = default;

  public:
    void*const ptr;
    const size_t size;
  };

  class HeapBuffer : public Buffer {
  public:
    NOOB_DL_API explicit HeapBuffer(size_t size);
    NOOB_DL_API bool localAccessible() override;

  protected:
    NOOB_DL_API ~HeapBuffer();
  };
}
