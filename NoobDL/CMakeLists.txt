cmake_minimum_required(VERSION 3.3)
project(NoobDL)

set(CMAKE_CXX_STANDARD 14)
add_definitions(-DNOOB_DL_BUILDING=1)

add_library(NoobDL SHARED
        # util
        include/NoobDL/Utils/DLL.h
        include/NoobDL/Utils/SubclassCheck.h
        include/NoobDL/Utils/Defer.h
        include/NoobDL/Utils/Sprint.h
        include/NoobDL/Utils/ShapeCapacity.h
        include/NoobDL/Utils/ConstantFuture.h
        include/NoobDL/Utils/STLUtils.h

        # lib
        include/NoobDL/RefCountedObj.h src/RefCountedObj.cc
        include/NoobDL/DType.h
        include/NoobDL/Device.h
        include/NoobDL/Buffer.h src/Buffer.cc
        include/NoobDL/Symbol.h src/Symbol.cc
        include/NoobDL/Graph.h src/Graph.cc
        include/NoobDL/ComputeTask.h src/ComputeTask.cc

        # syms
        include/NoobDL/Syms/Placeholder.h
        include/NoobDL/Syms/Variable.h
        include/NoobDL/Syms/Fill.h
        include/NoobDL/Syms/Add.h
        include/NoobDL/Syms/Mul.h
        )

target_include_directories(NoobDL PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)

if (COMMAND cotire)
    cotire(NoobDL)
endif ()
