//
// by afpro.
//

#include <atomic>
#include <NoobDL/Symbol.h>
#include <NoobDL/Graph.h>
#include <NoobDL/Utils/Sprint.h>

using namespace NoobDL;

std::string Symbol::autoName() {
  static std::atomic_ulong autoNameId(0);
  return Utils::sprint("sym_%lu", static_cast<unsigned long>(autoNameId++));
}
