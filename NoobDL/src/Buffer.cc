//
// by afpro.
//

#include <NoobDL/Buffer.h>

using namespace NoobDL;

HeapBuffer::HeapBuffer(size_t size)
  : Buffer(operator new(size), size) { }

HeapBuffer::~HeapBuffer() {
  operator delete(ptr);
}

bool HeapBuffer::localAccessible() {
  return true;
}
