//
// by afpro.
//

#include <set>
#include <condition_variable>
#include <mutex>
#include <algorithm>
#include <NoobDL/Device.h>
#include <NoobDL/Buffer.h>
#include <NoobDL/Graph.h>
#include <NoobDL/ComputeTask.h>
#include <NoobDL/Symbol.h>
#include <NoobDL/DType.h>
#include <NoobDL/Syms/Fill.h>
#include <NoobDL/Syms/Add.h>
#include <NoobDL/Syms/Mul.h>
#include <NoobDL/Utils/Sprint.h>
#include <NoobDL/Utils/STLUtils.h>
#include <NoobDL/Utils/ShapeCapacity.h>

using namespace NoobDL;

namespace {
  enum TaskType {
    C,
    D,
  };
}

struct ComputeTask::Task {
  const TaskType type;
  const Symbol* sym;
  bool executed;
  std::vector<Task*> dependBy;
  std::vector<Task*> dependOn;

  Task(TaskType type, Symbol* sym) : type(type), sym(sym), executed(false) { }

  bool done() {
    return std::all_of(
      dependBy.begin(), dependBy.end(),
      [](Task* task) { return task->executed; }
    );
  }

  bool depReady() {
    return std::all_of(
      dependOn.begin(), dependOn.end(),
      [](Task* task) { return task->executed; }
    );
  }
};

ComputeTask::ComputeTask(const RefPtr<Device>& device, const RefPtr<Graph>& graph)
  : device(device), graph(graph), mOriginThread(std::this_thread::get_id()), mExecuted(false) {}

ComputeTask::~ComputeTask() {
  for (auto task : mTasks) delete task;
}

void ComputeTask::checkThreadAndExecuteStatus(const char* message, bool desireExecuted) const {
  if (mOriginThread != std::this_thread::get_id())
    Utils::runtimeError("%s cross thread", message);

  if (desireExecuted != mExecuted)
    Utils::runtimeError("%s wrong execute status (desire %s, but %s)",
                        message, Utils::boolStr(desireExecuted), Utils::boolStr(mExecuted));
}

ComputeTask& ComputeTask::feed(const std::string& name, const RefPtr<Buffer>& data) {
  checkThreadAndExecuteStatus("feed");
  mFeeds[name] = data;
  return *this;
}

ComputeTask& ComputeTask::fetch(const std::string& name, const FetchCallback& callback) {
  checkThreadAndExecuteStatus("fetch");
  return *this;
}

ComputeTask& ComputeTask::derivative(const std::string& name, const std::string& target, const DerivativeCallback& callback) {
  checkThreadAndExecuteStatus("derivative");
  return *this;
}

void ComputeTask::run() const {
  checkThreadAndExecuteStatus("run");
}
