//
// by afpro.
//

#include <NoobDL/Graph.h>
#include <NoobDL/Symbol.h>
#include <NoobDL/Utils/Sprint.h>
#include <NoobDL/Utils/STLUtils.h>

using namespace NoobDL;

void Graph::syncRun(const std::function<void(Graph&)>& fun) {
  std::lock_guard<std::recursive_mutex> lock(mMut);
  fun(*this);
}

void Graph::addSymbol(RefPtr<Symbol> symbol) {
  if (!symbol)
    Utils::runtimeError("symbol empty");

  if (!Utils::mapContains(mSymbolMap, symbol->name))
    Utils::runtimeError("%s already on graph", symbol->name.c_str());

  for (const auto& input : symbol->inputs) {
    if (!Utils::mapContains(mSymbolMap, input))
      Utils::runtimeError("input %s of %s not found on graph",
                          input.c_str(), symbol->name.c_str());
  }

  mSymbolMap[symbol->name] = symbol;
  mSymbols.push_back(symbol);

  for (const auto& input : symbol->inputs) {
    mOutputMap[input].push_back(symbol->name);
  }
}

size_t Graph::symbolCount() const {
  return mSymbols.size();
}

RefPtr<Symbol> Graph::getSymbol(int index) const {
  return mSymbols[index];
}

RefPtr<Symbol> Graph::getSymbol(const std::string& name) const {
  return Utils::findMap<Symbol*, std::string, Symbol*>(
    mSymbolMap, name,
    [&](Symbol* sym) -> Symbol* { return sym; },
    [&]() -> Symbol* {
      Utils::runtimeError("%s not on graph", name.c_str());
      return nullptr;
    });
}

std::vector<std::string> Graph::inputOf(const std::string& name) const {
  using vec = std::vector<std::string>;
  return Utils::findMap<vec, std::string, Symbol*>(
    mSymbolMap, name,
    [&](Symbol* sym) -> vec { return sym->inputs; },
    [&]() -> vec {
      Utils::runtimeError("%s not on graph", name.c_str());
      return vec();
    });
}

std::vector<std::string> Graph::outputOf(const std::string& name) const {
  using vec = std::vector<std::string>;
  return Utils::findMap<vec, std::string, vec>(
    mOutputMap, name,
    [&](const vec& outputs) -> vec { return outputs; },
    [&]() -> vec {
      Utils::runtimeError("%s not on graph", name.c_str());
      return vec();
    });
}
