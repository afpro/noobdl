//
// by afpro.
//

#include <NoobDL/RefCountedObj.h>

using namespace NoobDL;

RefCountedObj::RefCountedObj() : mRef(0) { }

RefCountedObj::~RefCountedObj() { }

void RefCountedObj::incRef() {
  ++mRef;
}

void RefCountedObj::decRef() {
  if (--mRef == 0)
    deleteIt();
}

void RefCountedObj::deleteIt() {
  delete this;
}
